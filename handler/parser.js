'use strict';

let s3Helper = require('./shared/s3Helper');

/**
 * Parsing comments [content] from s3 bucket store.
 * @param event 
 * @param context
 * @param callback
 */
module.exports.parseComment = (event, context, callback) => {
    let record = event.Records[0];
    let bucketName = record.s3.bucket.name;
    let fileName = record.s3.object.key;
    console.log('retreiving file ' + fileName + ' from ' + bucketName);
    // Get file from s3
    s3Helper.getFile(bucketName, fileName, (err, data) => {
        if(err) {
            console.error(err);
            return;
        }

        console.log(JSON.parse(data.Body.toString()));
        callback();
    });
};