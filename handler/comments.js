'use strict';

let fs = require('fs'),
    uuidV4 = require('uuid/v4');

let s3Helper = require('./shared/s3Helper'),
  responseHelper = require('./shared/responseHelper'),
  validator = require('./shared/validator'),
  serviceHelper = require('./shared/serviceHelper');

/**
 * Gets comment from the service
 * Sends the comment to the client
 * Writes the comment to the local temp file
 * Uploads to s3 bucket
 */
module.exports.getComment = (event, context, callback) => {
  let id = event.pathParameters.id;
  console.log('Received request to get comment for ' + id);

  // Validates id value
  let err = validator.validateInteger('id', id);
  if (err) {
    callback(null, responseHelper.getHttpResponseObject(responseHelper.StatusCodes.BAD_REQUEST, err));
    return;
  }

  let fileName = uuidV4() + '.json';
  let filePath = '/tmp/' + fileName;
  let fileStream = fs.createWriteStream(filePath);
  console.log('The temp file name is: ' + fileName);

  // Gets the comment from the service.
  serviceHelper.getCommentById(id, (err, response) => {
    if (err) {
      callback(null, err);
      return;
    }

    // Returns the json comment to the client.
    callback(null, response);
    // Writes the same in a temp file
    fileStream.write(response.body);
    // uploads file to s3
    s3Helper.uploadFile(process.env.BUCKET_NAME, filePath, 'comments/' + fileName, function (err) {
      if (err) {
        console.error(err);
        return;
      }
      console.log('comment #' + id + 'in ' + fileName + ' uploaded to s3');
    });
  });

};