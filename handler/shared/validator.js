'use strict';

let _ = require('lodash');

/**
 * Validates id whether it is an integer or not.
 * @param id
 */
module.exports.validateInteger = (key, id) => {
  id = id * 1;

  if (_.isNaN(id) || !_.isInteger(id)) {
    console.warn(id + ' is not an integer');
    return {
        error: key + ' should be an integer'
    };
  }
};