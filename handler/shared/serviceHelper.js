'use strict';

let request = require('request');

let responseHelper = require('./responseHelper');

module.exports.getCommentById = (id, callback) => {

    request
        .get({
            baseUrl: process.env.SERVICE_BASE_URL,
            uri: 'comments/' + id
        })
        .on('response', (response) => {

            let err = validateServiceResponse(response);
            if (err) {
                callback(err);
                return;
            }

            response.on('data', (chunks) => {
                let response = responseHelper.getHttpResponseObject(responseHelper.StatusCodes.OK, chunks.toString());
                callback(null, response);
            });

        })
        .on('error', (error) => {
            console.error(error);
            callback(responseHelper.getHttpResponseObject(responseHelper.StatusCodes.INTERNAL_SERVER_ERROR, { error: 'Unknown error occurred' }));
        });

};


let validateServiceResponse = (response) => {
    if (response.statusCode === responseHelper.StatusCodes.OK) {
        return null;
    }

    if (response.statusCode === responseHelper.StatusCodes.NOT_FOUND) {
        return responseHelper.getHttpResponseObject(responseHelper.StatusCodes.NOT_FOUND, { error: 'Comment not found' });
    }

    console.error(response.statusCode);
    return responseHelper.getHttpResponseObject(responseHelper.StatusCodes.INTERNAL_SERVER_ERROR, { error: 'Unknown error occurred' });
};