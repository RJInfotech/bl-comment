'use strict';
/**
 * different http response status codes
 * @type {{BAD_REQUEST: number, OK: number, NOT_FOUND: number, INTERNAL_SERVER_ERROR: number}}
 */
module.exports.StatusCodes = {
    OK: 200,
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
    INTERNAL_SERVER_ERROR: 500
};

module.exports.getHttpResponseObject = (statusCode, body) => {
    return {
        statusCode: statusCode,
        body: typeof body !== 'string' ? JSON.stringify(body) : body
    };
};