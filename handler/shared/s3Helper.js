'use strict';

var AWS = require('aws-sdk'),
    fs = require('fs');

/**
 * Uploads file to s3
 * @param bucketName    Name of the bucket, under which the file would be uploaded.
 * @param fileName      Name of the file, which will be uploaded to s3 bucket.
 * @param locationKey   Location key, which would be used as a key for the file in s3 bucket.
 * @param callback      Function to be called, once it is done.
 */
module.exports.uploadFile = function (bucketName, fileName, locationKey, callback) {
    let s3 = new AWS.S3();
    let fileStream = fs.createReadStream(fileName);
    s3.putObject({
        Key: locationKey,
        Body: fileStream,
        Bucket: bucketName,
        ContentType: 'application/json'
    }, callback);
};

/**
 * Gets file from s3
 * @param bucketName    Name of the bucket, under which the file would be uploaded.
 * @param fileName      Name of the file, which will be used as a key in s3 bucket.
 * @param callback      Function to be called, once it is done.
 */
module.exports.getFile = function (bucketName, fileName, callback) {
    let s3 = new AWS.S3();
    s3.getObject({
        Key: fileName,
        Bucket: bucketName
    }, callback);
};