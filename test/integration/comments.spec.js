let request = require('supertest'),
    expect = require('chai').expect;

let base_url = process.env.BL_BASE_URL;

describe('Get /comments/{id}', () => {
    if(!base_url) {
        base_url = 'https://zmz148irhi.execute-api.ap-southeast-1.amazonaws.com/dev';
    }

    it('Should return comment for a valid id', (done) => {
        request(base_url)
            .get('/comments/10')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).not.to.be.null;
                expect(res.statusCode).to.be.equal(200);
                expect(res.body).not.to.be.null;
                expect(res.body.id).to.be.equal(10);
                expect(res.body.email).not.to.be.null;
                expect(res.body.body).not.to.be.null;
                done();
            });
    });
    
    it('Should return 400 BAD REQUEST for an invalid id', (done) => {
        request(base_url)
            .get('/comments/10a')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).not.to.be.null;
                expect(res.statusCode).to.be.equal(400);
                expect(res.body.error).to.be.equal('id should be an integer');
                done();
            });
    });
    
    it('Should return 404 NOT FOUND for an non-exist id', (done) => {
        request(base_url)
            .get('/comments/1000')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).not.to.be.null;
                expect(res.statusCode).to.be.equal(404);                
                expect(res.body.error).to.be.equal('Comment not found');
                done();
            });
    });
});