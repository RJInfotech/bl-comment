let expect = require('chai').expect,
    sinon = require('sinon'),
    proxyquire = require('proxyquire');

let comments = require('../../handler/comments');

describe('comments', () => {
    describe('getComment', () => {
        describe('For invalid id', () => {
            let err;
            beforeEach((done) => {
                comments.getComment({ pathParameters: { id: 'abcd' } }, null, (e, r) => {
                    err = r;
                    done();
                });
            });
            it('should return 400', () => {
                expect(err).to.not.be.null;
                expect(err.statusCode).to.be.equal(400);
            });
        });

        describe('For valid id', () => {

            describe('Error occurred in service', () => {
                let err;
                beforeEach((done) => {
                    let serviceHelper = {
                        getCommentById: function (id, cb) {
                            cb({ statusCode: 500, body: 'an error' });
                        }
                    }
                    comments = proxyquire('../../handler/comments', {
                        './shared/serviceHelper': serviceHelper
                    });
                    comments.getComment({ pathParameters: { id: '123' } }, null, (e, r) => {
                        err = r;
                        done();
                    })
                });
                it('should return same error code in response', () => {
                    expect(err).to.not.be.null;
                    expect(err.statusCode).to.be.equal(500);
                });
            });

            describe('service returns valid response', () => {
                let response;
                beforeEach((done) => {
                    let serviceHelper = {
                        getCommentById: function (id, cb) {
                            cb({ statusCode: 200, body: 'sample data' });
                        }
                    };

                    let s3Helper = {
                        uploadFile: (bucketName, fileName, keyName) => {
                            console.log('requested to upload to s3');
                        }
                    };

                    let fs = {
                        createWriteStream: (filePath) => {
                            console.log('should create write stream');
                        },
                        write: (content) => {
                            console.log('should write content');
                        }
                    }
                    comments = proxyquire('../../handler/comments', {
                        './shared/serviceHelper': serviceHelper,
                        './shared/s3Helper': s3Helper,
                        'fs': fs
                    });
                    comments.getComment({ pathParameters: { id: '123' } }, null, (e, r) => {
                        response = r;
                        done();
                    })
                });

                it('should return 200 in response along with body', () => {
                    expect(response).to.not.be.null;
                    expect(response.statusCode).to.be.equal(200);
                    expect(response.body).to.be.string;
                });
            });
        });
    });
});