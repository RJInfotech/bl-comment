let proxyquire = require('proxyquire'),
    sinon = require('sinon'),
    expect = require('chai').expect;

describe('parser', () => {
    beforeEach(() => {
       sinon.stub(console, 'log');
    });
    describe('parseComment', () => {
        beforeEach((done) => {
            let s3Helper = {
                getFile: function (bn, fn, cb) {
                    cb(null, {
                        Body: '{ "sample": "text"}'
                    });
                }
            };

            let parser = proxyquire('../../handler/parser', {
                './shared/s3Helper': s3Helper
            });

            parser.parseComment({
                Records: [
                    {
                        s3: {
                            bucket: {
                                name: 'test'
                            },
                            object: {
                                key: 'sample.txt'
                            }
                        }
                    }
                ]
            }, null, done);
        });

        it('should write a text in console', () => {
            expect(console.log.calledTwice).to.be.true;
            console.log.restore();
        });
    });
});