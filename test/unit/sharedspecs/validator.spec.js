let expect = require('chai').expect;

let validator = require('../../../handler/shared/validator');

describe('validator', () => {
    describe('validateInteger', () => {
        it('For an non-numeric value, it should return error', () => {
            let result = validator.validateInteger('a', 'ab234');
            expect(result).to.not.be.null;
            expect(result.error).to.not.be.null;
        });
        it('For an numeric (float) value, it should return error', () => {
            let result = validator.validateInteger('a', '23.4');
            expect(result).to.not.be.null;
            expect(result.error).to.not.be.null;
        });
        it('For an integer value, it should NOT return error', () => {
            let result = validator.validateInteger('a', '234');
            expect(result).to.be.undefined;
        });
    });
});