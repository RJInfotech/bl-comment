# BL Comment App #

### What is this repository for? ###

* Comment application for BL
* 1.0.0

### Global installation ###

* serverless
* jshint (since, can be used for all projects)

### How do I get set up? ###
* npm install - to install all node packages
* serverless config credential - to setup aws credential to be used for deploy
* serverless deploy - to deploy into aws
* jshint - to analyze and verify the source code if it complies with coding rules.


### How do I run the test? ###

npm test

* covers unit tests using mocha
* covers integration tests using supertest.

*As 'inbox' bucket name has been used by someone else and it is unavailable for us.  I've used 'inb8x' for the testing.* 


### Who do I talk to? ###

* Ramji Piramanayagam